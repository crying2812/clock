import './assets/styles/app.css';
import DatePicker from "./components/DatePicker";

function onDateChange(t) {
    console.log(t);
}

function App() {

    return (
        <div className="container">
            <fieldset>
                <legend>Date mobile</legend>
                <DatePicker onChange={onDateChange}/>
            </fieldset>

            <fieldset>
                <legend>Time</legend>

            </fieldset>
        </div>
    );
}

export default App;

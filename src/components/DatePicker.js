import React, {useEffect, useRef, useState} from 'react';

import '../assets/styles/date.css';

export default function DatePicker(props) {

    const dateObj = new Date();

    const lstStringDays = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];

    const lstStringMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    let dayAsTimestamp = 60 * 60 * 24 * 1000;

    let todayTimestamp = Date.now() - (Date.now() % dayAsTimestamp) + (new Date().getTimezoneOffset() * 1000 * 60);

    const getMonthDetails = (year, month) => {
        let firstDay = (new Date(year, month)).getDay();

        let numberOfDays = getNumberOfDays(year, month);

        let monthArray = [];

        let rows = 6;

        let currentDay = null;

        let index = 0;

        let cols = 7;

        for (let row = 0; row < rows; row++) {
            for (let col = 0; col < cols; col++) {
                currentDay = getDayDetails({
                    index,
                    numberOfDays,
                    firstDay,
                    year,
                    month
                });
                monthArray.push(currentDay);
                index++;
            }
        }
        return monthArray;
    }

    const getNumberOfDays = (year, month) => {
        return 40 - new Date(year, month, 40).getDate();
    }

    const getDayDetails = args => {
        let date = args.index - args.firstDay;

        let day = args.index % 7;

        let prevMonth = args.month - 1;

        let prevYear = args.year;

        if (prevMonth < 0) {
            prevMonth = 11;

            prevYear--;
        }

        let prevMonthNumberOfDays = getNumberOfDays(prevYear, prevMonth);

        let _date = (date < 0 ? prevMonthNumberOfDays + date : date % args.numberOfDays) + 1;

        let month = date < 0 ? -1 : date >= args.numberOfDays ? 1 : 0;

        let timestamp = new Date(args.year, args.month, _date).getTime();

        return {
            date: _date,
            day,
            month,
            timestamp,
            dayString: lstStringDays[day]
        }
    }

    const isCurrentDay = day => {
        return day.timestamp === todayTimestamp;
    }

    const isSelectedDay = day => {
        return day.timestamp === selectedDay;
    }

    const [selectedDay, setSelectedDay] = useState(todayTimestamp);

    const [month, setMonth] = useState(dateObj.getMonth());

    const [year, setYear] = useState(dateObj.getFullYear());

    const [monthsOfYear, setMonthsOfYear] = useState(getMonthDetails(year, month));

    const [showModal, setShowModal] = useState(false);

    const datePickerRef = useRef(null);

    const inputRef = useRef(null);

    useEffect(() => {
        setDateToInput(selectedDay)

        function onClickAway(event) {
            if (datePickerRef.current && !datePickerRef.current.contains(event.target)) {
                setShowModal(false);
            }
        }

        document.addEventListener("mousedown", onClickAway);

        return () => {
            document.removeEventListener("mousedown", onClickAway);
        };
    }, [datePickerRef]);

    const getDateFromDateString = dateValue => {
        let dateData = dateValue.split('/').map(d => parseInt(d, 10));
        if (dateData.length < 3)
            return null;

        let year = dateData[0];

        let month = dateData[1];

        let date = dateData[2];

        return {date, month, year};
    }

    const getMonthStr = month => lstStringMonths[Math.max(Math.min(11, month), 0)] || 'Month';

    const getDateStringFromTimestamp = timestamp => {
        let dateObject = new Date(timestamp);

        let month = dateObject.getMonth() + 1;

        let date = dateObject.getDate();

        return (date < 10 ? '0' + date : date) + '/' + (month < 10 ? '0' + month : month) + '/' + dateObject.getFullYear();
    }

    const setDate = dateData => {
        setSelectedDay(new Date(dateData.year, dateData.month - 1, dateData.date).getTime());

        if (props.onChange) {
            props.onChange(selectedDay);
        }
    }

    const onInputChange = () => {
        let dateValue = inputRef.current.value;

        let dateData = getDateFromDateString(dateValue);

        setDate(dateData);

        setYear(dateData.year);

        setMonth(dateData.month - 1);

        setMonthsOfYear(getMonthDetails(dateData.year, dateData.month - 1));
    }

    const setDateToInput = (timestamp) => {
        inputRef.current.value = getDateStringFromTimestamp(timestamp);
    }

    const onDateClick = day => {
        setSelectedDay(day.timestamp)
        setDateToInput(day.timestamp)
        props.onChange(day.timestamp);
        setShowModal(false)
    }

    const setCurrentYear = (value) => {
        setYear(value)
        setMonthsOfYear(getMonthDetails(value, month))
    }

    const setCurrentMonth = (offset) => {
        let tmpYear = year;

        let tmpMonth = month + offset;

        if (tmpMonth === -1) {

            tmpMonth = 11;

            tmpYear--;

        } else if (tmpMonth === 12) {
            tmpMonth = 0;

            tmpYear++;
        }

        setYear(tmpYear)

        setMonth(tmpMonth)

        setMonthsOfYear(getMonthDetails(tmpYear, tmpMonth))
    }

    const renderBody = () => {
        let days = monthsOfYear.map((day, index) => {
            return (
                <div className={'date__picker--item ' + (day.month !== 0 ? ' disabled' : '') +
                (isCurrentDay(day) ? ' current' : '') + (isSelectedDay(day) ? ' selected' : '')} key={index}
                     onClick={() => onDateClick(day)}>
                    {day.date}
                </div>
            )
        })

        return (
            <div className='date__picker--calendar'>
                <div className='date__picker--calendarHeading'>
                    {lstStringDays.map((d, i) => <div key={i} className='month'>{d}</div>)}
                </div>
                <div className='date__picker--calendarBody'>
                    {days}
                </div>
            </div>
        )
    }

    const onYearChange = (e) => {
        setCurrentYear(e.target.value)
    }

    const renderYears = () => {

        const options = []

        for (let i = 1970; i < 2100; i++) {
            options.push(<option key={i} value={i}>{i}</option>)
        }

        return <select onChange={onYearChange} value={year}>
            {options}
        </select>
    }

    return (
        <div className='date__picker' ref={datePickerRef}>
            <div className='date__picker--input' onClick={() => setShowModal(true)}>
                <input type='text' onKeyDown={(e)=> e.preventDefault()} onChange={onInputChange} ref={inputRef}/>
            </div>
            {showModal ? (
                <div className='date__picker--modal'>
                    <div className='date__picker--top'>
                        <div className='date__picker--top--left'>
                            <div className='date__picker--currentMonth'>{getMonthStr(month)}</div>
                            <div className='date__picker--currentYear'>{renderYears()}
                            </div>
                        </div>
                        <div className="date__picker--top--right">
                            <div className='date__picker--nav' onClick={() => setCurrentMonth(-1)}>
                                <span className='date__picker--nav--prevMonth'/>
                            </div>
                            <div className='date__picker--nav' onClick={() => setCurrentMonth(1)}>
                                <span className='date__picker--nav--nextMonth'/>
                            </div>
                        </div>

                    </div>
                    <div className='date__picker--body'>
                        {renderBody()}
                    </div>
                </div>
            ) : ''}
        </div>
    )
}